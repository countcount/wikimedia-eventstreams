use core::fmt::Debug;
use std::{
    collections::HashSet,
    time::{Duration, SystemTime},
};

use es::{Client, ReconnectOptions};
use eventsource_client as es;
use futures::{Stream, StreamExt};
use http::status::StatusCode;
use thiserror::Error;
use time::{format_description::well_known::Rfc3339, OffsetDateTime};
use tracing::{debug, info, warn};

use wikimedia_events::{
    page_change::PageChange, page_create::PageCreate, page_delete::PageDelete,
    page_links_change::PageLinksChange, page_move::PageMove,
    page_properties_change::PagePropertiesChange, page_undelete::PageUndelete,
    recentchange::RecentChange, revision_create::RevisionCreate,
    revision_tags_change::RevisionTagsChange, revision_visibility_change::RevisionVisibilityChange,
    Any, EventData, NormalEventData,
};

/*
 * TODO:
 * - try reqwest-based stream lib
 * - handling server errors for invalid since/last id/other 400 errors
 */

#[derive(Error, Debug)]
#[error(transparent)]
pub struct EventSourceLibraryError(#[from] es::Error);

#[derive(Error, Debug)]
pub enum StreamError {
    #[error("Error from underlying event source library")]
    EventSourceLibrary(#[from] EventSourceLibraryError),
    #[error("Fatal error from underlying event source library")]
    EventSourceLibraryFatal,
    #[error("Deserialize Error")]
    Parse(#[from] serde_json::Error),
}

#[derive(Error, Debug)]
pub enum BuildError {
    #[error("Invalid user agent: {0}")]
    InvalidUserAgent(String),
    #[error("Invalid since parameter: {0}")]
    InvalidSince(String),
    #[error("At least one stream must be specified.")]
    NoStreamSpecified,
}

pub struct Builder {
    user_agent: String,
    read_timeout: Duration,
    since: Option<String>,
    last_event_id: Option<String>,
}

#[derive(Clone, PartialEq, Debug)]
pub struct Event<T: EventData> {
    pub id: Option<String>,
    pub data: T,
}

#[derive(Error, Debug)]
#[error(transparent)]
pub struct TimeError(#[from] time::error::Format);

impl Builder {
    pub fn new() -> Self {
        Self {
            user_agent: ("unknown/unknown (unknown) wikimedia-eventstreams (Rust)/".to_string()
                + env!("CARGO_PKG_VERSION")),
            read_timeout: Duration::from_secs(60),
            since: None,
            last_event_id: None,
        }
    }

    pub fn user_agent(mut self, user_agent: &str) -> Self {
        self.user_agent = user_agent.to_string();
        self
    }

    pub fn read_timeout(mut self, read_timeout: Duration) -> Self {
        self.read_timeout = read_timeout;
        self
    }

    pub fn since_javscript_time(mut self, javascript_time: String) -> Self {
        self.since = Some(javascript_time);
        self
    }

    pub fn since(mut self, ts: SystemTime) -> Result<Self, TimeError> {
        let d = OffsetDateTime::from(ts);
        self.since = Some(d.format(&Rfc3339)?);
        Ok(self)
    }

    pub fn since_ago(mut self, d: Duration) -> Result<Self, TimeError> {
        let d = OffsetDateTime::now_utc() - d;
        self.since = Some(d.format(&Rfc3339)?);
        Ok(self)
    }

    pub fn last_event_id(mut self, last_event_id: &str) -> Self {
        self.last_event_id = Some(last_event_id.to_string());
        self
    }

    pub fn build<T>(
        self,
    ) -> Result<impl Stream<Item = Result<Event<T>, StreamError>> + Send + Unpin, BuildError>
    where
        T: NormalEventData,
    {
        self.build_for_streams(T::STREAM)
    }

    fn build_for_streams<T>(
        self,
        streams: &str,
    ) -> Result<impl Stream<Item = Result<Event<T>, StreamError>> + Send + Unpin, BuildError>
    where
        T: EventData,
    {
        let mut url = "https://stream.wikimedia.org/v2/stream/".to_string() + streams;
        if let Some(since) = self.since {
            url = reqwest::Client::new()
                .get(url)
                .query(&[("since", since)])
                .build()
                .map_err(|err| BuildError::InvalidSince(err.to_string()))?
                .url()
                .to_string();
        }

        let mut client_builder = es::ClientBuilder::for_url(url.to_string().as_str())
            .expect("valid URL because it was checked before")
            .read_timeout(self.read_timeout)
            .reconnect(
                ReconnectOptions::reconnect(true)
                    // necessary because otherwise backoff is not honored if the
                    // initial reconnection fails
                    .retry_initial(true)
                    .build(),
            );
        client_builder = client_builder
            .header("User-Agent", self.user_agent.as_str())
            .map_err(|err| BuildError::InvalidUserAgent(err.to_string()))?;
        if let Some(last_event_id) = self.last_event_id {
            client_builder = client_builder.last_event_id(last_event_id);
        }
        let client = client_builder.build();

        let stream = client.stream();
        Ok(stream.filter_map(Self::map_events).boxed())
    }

    async fn map_events<T>(ev: Result<es::SSE, es::Error>) -> Option<Result<Event<T>, StreamError>>
    where
        T: EventData,
    {
        match ev {
            Ok(es::SSE::Comment(c)) => {
                debug!("Received comment {c}");
                None
            }
            Ok(es::SSE::Event(es::Event {
                event_type,
                data,
                id,
                ..
            })) if event_type == "message" => Some(
                serde_json::from_str::<T>(data.as_str())
                    .map(|data| Event { data, id })
                    .map_err(|e| e.into()),
            ),
            Ok(es::SSE::Event(es::Event {
                event_type, data, ..
            })) => {
                warn!("Received event of unexpected type {event_type} with data {data}");
                None
            }
            Ok(es::SSE::Connected(_details)) => None,
            Err(es::Error::Eof) => {
                // expected to happen
                debug!("EOF received");
                None
            }
            Err(es::Error::UnexpectedEof) => {
                info!("EOF received unexpectedly");
                None
            }
            Err(es::Error::TimedOut) => {
                info!("Timed out receiving events");
                None
            }
            Err(es::Error::UnexpectedResponse(r, _body))
                if r.status() == StatusCode::TOO_MANY_REQUESTS =>
            {
                warn!("Received 429 - too many requests");
                None
            }
            Err(es::Error::UnexpectedResponse(r, _body)) if r.status() >= 500 => {
                // expected to happen
                None
            }
            Err(es::Error::StreamClosed) => Some(Err(StreamError::EventSourceLibraryFatal)),
            Err(err) => Some(Err(EventSourceLibraryError::from(err).into())),
        }
    }

    pub fn build_for_multiple(
        self,
        streams: MultipleStreams,
    ) -> Result<impl Stream<Item = Result<Event<Any>, StreamError>> + Send + Unpin, BuildError>
    {
        let streams = streams.streams();
        if streams.is_empty() {
            return Err(BuildError::NoStreamSpecified);
        }
        self.build_for_streams::<Any>(streams.as_str())
    }
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}

pub struct MultipleStreams {
    streams: HashSet<&'static str>,
}

impl MultipleStreams {
    pub fn none() -> Self {
        Self {
            streams: HashSet::new(),
        }
    }

    pub fn all() -> Self {
        let mut streams = Self::none();
        streams = streams
            .add::<PageChange>()
            .add::<PageCreate>()
            .add::<PageDelete>()
            .add::<PageLinksChange>()
            .add::<PageMove>()
            .add::<PagePropertiesChange>()
            .add::<PageUndelete>()
            .add::<RecentChange>()
            .add::<RevisionCreate>()
            .add::<RevisionTagsChange>()
            .add::<RevisionVisibilityChange>();
        streams
    }

    pub fn add<T: NormalEventData>(mut self) -> Self {
        self.streams.insert(T::STREAM);
        self
    }

    pub fn remove<T: NormalEventData>(mut self) -> Self {
        self.streams.remove(T::STREAM);
        self
    }

    fn streams(self) -> String {
        self.streams.into_iter().collect::<Vec<_>>().join(",")
    }
}

impl Default for MultipleStreams {
    fn default() -> Self {
        Self::none()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::{pin_mut, TryStreamExt};
    use wikimedia_events::{page_change::PageChange, NormalEventData};

    async fn test_events<T: NormalEventData>(check_schema: bool) {
        let stream = Builder::new()
            .since_ago(Duration::from_secs(60 * 30))
            .unwrap()
            .build::<T>()
            .unwrap();
        stream
            .take(1)
            .try_for_each(|ev| async move {
                if check_schema {
                    assert_eq!(T::SCHEMA, ev.data.event_schema());
                }
                println!("{:?}", ev);
                Ok(())
            })
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_page_change_events() {
        test_events::<PageChange>(true).await;
    }

    #[tokio::test]
    async fn test_page_create_events() {
        test_events::<PageCreate>(true).await;
    }

    #[tokio::test]
    async fn test_page_delete_events() {
        test_events::<PageDelete>(true).await;
    }

    #[tokio::test]
    async fn test_page_links_change_events() {
        test_events::<PageLinksChange>(true).await;
    }

    #[tokio::test]
    async fn test_page_move_events() {
        test_events::<PageMove>(true).await;
    }

    #[tokio::test]
    async fn test_page_properties_change_events() {
        test_events::<PagePropertiesChange>(true).await;
    }

    #[tokio::test]
    async fn test_page_undelete_events() {
        test_events::<PageUndelete>(true).await;
    }

    #[tokio::test]
    async fn test_recent_change_events() {
        // sometimes events of version 1.0.0 are delivered, sometimes it's 1.0.1
        test_events::<RecentChange>(false).await;
    }

    #[tokio::test]
    async fn test_revision_create_events() {
        test_events::<RevisionCreate>(true).await;
    }

    #[tokio::test]
    async fn test_revision_tags_change_events() {
        test_events::<RevisionTagsChange>(true).await;
    }

    // does not deliver events, privileged?
    #[ignore]
    #[tokio::test]
    async fn test_revision_visibility_change_events() {
        test_events::<RevisionVisibilityChange>(true).await;
    }

    #[tokio::test]
    async fn test_try_next() {
        let mut stream = Builder::new()
            .since_ago(Duration::from_secs(60 * 30))
            .unwrap()
            .build::<RecentChange>()
            .unwrap()
            .take(10);
        while stream.try_next().await.unwrap().is_some() {}
    }

    #[tokio::test]
    async fn test_multi() {
        let start = SystemTime::now() - Duration::from_secs(60 * 30);
        let streams = MultipleStreams::none()
            .add::<RecentChange>()
            .add::<RevisionCreate>();
        let mut stream = Builder::new()
            .since(start)
            .unwrap()
            .build_for_multiple(streams)
            .unwrap()
            .boxed();
        stream.try_next().await.unwrap().unwrap();
    }

    #[tokio::test]
    async fn test_multi_all() {
        let start = SystemTime::now() - Duration::from_secs(60 * 60);
        let streams = MultipleStreams::all();
        Builder::new()
            .since(start)
            .unwrap()
            .build_for_multiple(streams)
            .unwrap()
            .take(1000)
            .try_for_each(|ev| async move {
                println!("{:?}", ev);
                Ok(())
            })
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_last_event_id() {
        let start = SystemTime::now() - Duration::from_secs(60 * 30);
        let mut stream = Builder::new()
            .since(start)
            .unwrap()
            .build::<RecentChange>()
            .unwrap()
            .boxed();
        let first_id = stream.try_next().await.unwrap().unwrap().id.unwrap();
        let mut stream2 = Builder::new()
            .since(start)
            .unwrap()
            .last_event_id(first_id.as_str())
            .build::<RecentChange>()
            .unwrap()
            .boxed();
        let next_id = stream2.try_next().await.unwrap().unwrap().id.unwrap();
        assert_ne!(first_id, next_id);
    }

    async fn test_timestamp(b: Builder, start: SystemTime) {
        let stream = b
            .build::<RecentChange>()
            .unwrap()
            .boxed()
            .try_filter_map(|e| async move { Ok(e.data.timestamp) });
        pin_mut!(stream);
        let ts = stream
            .try_next()
            .await
            .expect("should succeed")
            .expect("should return an event");
        let ts = SystemTime::UNIX_EPOCH + Duration::from_secs(ts as u64);
        let since_start = ts
            .duration_since(start)
            .expect("first event ts should be newer than given since timestamp");
        assert!(
            since_start < Duration::from_secs(2 * 60 * 60),
            "time difference should be less than 2 hours but is {:?}",
            since_start
        );
    }

    #[tokio::test]
    async fn test_since() {
        let start = SystemTime::now() - Duration::from_secs(2 * 24 * 60 * 60);
        let b = Builder::new().since(start).unwrap();
        test_timestamp(b, start).await;
    }

    #[tokio::test]
    async fn test_since_ago() {
        let secs = 2 * 24 * 60 * 60;
        let start = SystemTime::now() - Duration::from_secs(secs);
        let b = Builder::new().since_ago(Duration::from_secs(secs)).unwrap();
        test_timestamp(b, start).await;
    }
}
