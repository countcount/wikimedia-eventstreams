use anyhow::{bail, Context, Result};
use reqwest::Client;
use std::fmt::Write;
use std::{path::Path, process::Stdio, str::from_utf8};
use tempfile::{tempdir, TempDir};
use tokio::process::Command;

// Preqrequisites:
// cargo install schemafy --git https://github.com/Marwes/schemafy.git --rev d03858bec5537f355199f48d8cfd562df4a0e840 --bin schemafy --features tool

async fn get_latest_schema_file(url: &str) -> Result<String> {
    let content = Client::default()
        .get(url)
        .send()
        .await?
        .error_for_status()?
        .text()
        .await?;
    Ok(content)
}

async fn generate_src(
    tmp_dir: &TempDir,
    sub_url: &str,
    stream: &str,
    src_dir: &Path,
    src_file: &str,
    root_type: &str,
) -> Result<()> {
    let url = "https://schema.wikimedia.org/repositories/primary/jsonschema/mediawiki/".to_owned()
        + sub_url;
    let content = get_latest_schema_file(&url).await?;
    let json_value: serde_json::Value = serde_yaml::from_str(&content)?;
    let schema_id = json_value
        .get("$id")
        .context("no schema id")?
        .as_str()
        .context("schema id is not text")?;
    let json_string = serde_json::to_string_pretty(&json_value)?;
    let tmp_file = tmp_dir.path().join(src_file.to_owned() + ".json");
    std::fs::write(&tmp_file, json_string)?;
    let child = Command::new("schemafy")
        .arg(tmp_file)
        .arg("--root")
        .arg(root_type)
        .stdout(Stdio::piped())
        .spawn()?;
    let output = child.wait_with_output().await?;
    if !output.status.success() {
        bail!("schemafy exited with status {}", output.status)
    }
    let tmp_src_file = tmp_dir.path().join(src_file);
    let mut contents_buf = String::new();
    write!(
        contents_buf,
        "use serde::{{Deserialize, Serialize}};

impl super::NormalEventData for {root_type} {{
    const SCHEMA: &'static str = \"{schema_id}\";
    const STREAM: &'static str = \"mediawiki.{stream}\";

    fn event_schema(&self) -> &str {{
        self.schema.as_str()
    }}
}}

"
    )
    .unwrap();
    contents_buf.push_str(from_utf8(&output.stdout)?);
    std::fs::write(&tmp_src_file, contents_buf)?;
    std::fs::rename(tmp_src_file, src_dir.join(src_file))?;
    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    let src_dir = std::env::current_exe()?
        .parent()
        .context("could not get parent dir of current executable")?
        .join("../../wikimedia-events/src");
    let tmp_dir = tempdir()?;

    let mappings = [
        (
            "revision/create/2.0.0",
            "page_create.rs",
            "page-create",
            "PageCreate",
        ),
        (
            "page/change/1.2.0",
            "page_change.rs",
            "page_change.v1",
            "PageChange",
        ),
        (
            "page/delete/1.0.0",
            "page_delete.rs",
            "page-delete",
            "PageDelete",
        ),
        ("page/move/1.0.0", "page_move.rs", "page-move", "PageMove"),
        (
            "page/properties-change/1.0.0",
            "page_properties_change.rs",
            "page-properties-change",
            "PagePropertiesChange",
        ),
        (
            "page/undelete/1.0.0",
            "page_undelete.rs",
            "page-undelete",
            "PageUndelete",
        ),
        (
            "recentchange/1.0.1",
            "recentchange.rs",
            "recentchange",
            "RecentChange",
        ),
        (
            "page/links-change/1.0.0",
            "page_links_change.rs",
            "page-links-change",
            "PageLinksChange",
        ),
        (
            "revision/create/2.0.0",
            "revision_create.rs",
            "revision-create",
            "RevisionCreate",
        ),
        (
            "revision/tags-change/1.0.0",
            "revision_tags_change.rs",
            "revision-tags-change",
            "RevisionTagsChange",
        ),
        (
            "revision/visibility-change/1.0.0",
            "revision_visibility_change.rs",
            "revision-visibility-change",
            "RevisionVisibilityChange",
        ),
    ];

    for (sub_url, src_file, stream, root_type) in mappings {
        generate_src(&tmp_dir, sub_url, stream, &src_dir, src_file, root_type).await?;
    }
    Ok(())
}
