use std::{
    cell::RefCell,
    io::{stdout, Write},
};

use futures::TryStreamExt;
use tracing::Level;
use tracing_subscriber::{filter, prelude::*};
use wikimedia_eventstreams::{Builder, MultipleStreams};

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::WARN)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    init_logging();
    let count = RefCell::new(0);
    Builder::new()
        .user_agent("rcstream/0.0.1")
        .build_for_multiple(MultipleStreams::all())
        .unwrap()
        .try_for_each(|_| async {
            let mut b = count.borrow_mut();
            *b += 1;
            if *b % 10000 == 0 {
                print!("\r{}", b);
                stdout().flush().unwrap();
            }
            Ok(())
        })
        .await
        .unwrap();
}
