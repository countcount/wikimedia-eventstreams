use std::{
    cell::{RefCell, RefMut},
    collections::HashMap,
};

use futures::StreamExt;
use tracing::Level;
use tracing_subscriber::{filter, prelude::*};
use wikimedia_events::page_links_change::{PageLinksChange, PageLinksChangePerformer};
use wikimedia_eventstreams::Builder;

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::WARN)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    init_logging();
    let link_to_wiki: RefCell<HashMap<String, String>> = RefCell::new(HashMap::new());
    Builder::new()
        .user_agent("el/0.0.1")
        .build::<PageLinksChange>()
        .unwrap()
        .for_each(|ev| async {
            match ev {
                Ok(ev) => {
                    process_ev(link_to_wiki.borrow_mut(), ev.data);
                }
                Err(e) => println!("Error: {:?}", e),
            }
        })
        .await;
}

fn is_in_trusted_group(performer: &PageLinksChangePerformer) -> bool {
    performer
        .user_groups
        .iter()
        .any(|group| matches!(group.as_str(), "sysop" | "rollbacker" | "patroller"))
}

fn is_trusted_user(performer: &PageLinksChangePerformer) -> bool {
    performer.user_is_bot
        || (performer.user_id.is_some() && performer.user_edit_count.is_some_and(|ec| ec > 200))
        || is_in_trusted_group(performer)
}

fn process_ev(mut link_to_wiki: RefMut<HashMap<String, String>>, ev: PageLinksChange) {
    let (Some(domain), Some(added_links), Some(performer)) = (
        ev.meta.domain.as_ref(),
        ev.added_links,
        ev.performer.as_ref(),
    ) else {
        return;
    };
    if is_trusted_user(performer) {
        return;
    }
    for added_el in added_links
        .into_iter()
        .filter(|link| link.external == Some(true))
    {
        let Some(url) = added_el.link else {
            continue;
        };

        let e = link_to_wiki.entry(url);
        match e {
            std::collections::hash_map::Entry::Occupied(ref o) => {
                if o.get() != domain {
                    println!(
                        "{} - {} - https://{domain}/w/index.php?diff={} - ({:?},{:?})",
                        e.key(),
                        performer.user_text,
                        ev.rev_id,
                        ev.meta.id,
                        ev.meta.request_id
                    );
                    println!("    {}", o.get());
                    println!("    {domain}");
                }
            }
            std::collections::hash_map::Entry::Vacant(_) => {
                if domain == "de.wikipedia.org" {
                    println!(
                        "DEWIKI {} - {} - https://{domain}/w/index.php?diff={}",
                        e.key(),
                        performer.user_text,
                        ev.rev_id
                    );
                }
                e.or_insert(domain.clone());
            }
        };
    }
}
