use futures::StreamExt;
use tracing::Level;
use tracing_subscriber::{filter, prelude::*};
use wikimedia_events::page_links_change::PageLinksChange;
use wikimedia_eventstreams::Builder;

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::WARN)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    init_logging();
    Builder::new()
        .user_agent("el/0.0.1")
        .build::<PageLinksChange>()
        .unwrap()
        .for_each(|ev| async {
            match ev {
                Ok(ev) => {
                    process_ev(ev.data);
                }
                Err(e) => println!("Error: {:?}", e),
            }
        })
        .await;
}

fn process_ev(ev: PageLinksChange) {
    let Some("de.wikipedia.org") = ev.meta.domain.as_deref() else {
        return;
    };
    let Some(added_links) = ev.added_links.as_ref() else {
        return;
    };
    let mut first = true;
    for added_el in added_links
        .iter()
        .filter(|link| link.external == Some(true))
    {
        if first {
            println!("https://de.wikipedia.org/w/index.php?diff={}", ev.rev_id);
            first = false;
        }
        let Some(url) = added_el.link.as_ref() else {
            continue;
        };

        println!("    {}", url);
    }

    if !first {
        println!();
    }
}
