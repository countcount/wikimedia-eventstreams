use serde::{Deserialize, Serialize};

impl super::NormalEventData for PageLinksChange {
    const SCHEMA: &'static str = "/mediawiki/page/links-change/1.0.0";
    const STREAM: &'static str = "mediawiki.page-links-change";

    fn event_schema(&self) -> &str {
        self.schema.as_str()
    }
}

#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct PageLinksChangeItemAddedLinks {
    #[doc = " Whether link is external"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub external: Option<bool>,
    #[doc = " Link being added"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageLinksChangeMeta {
    #[doc = " Domain the event or entity pertains to"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[doc = " UTC event datetime, in ISO-8601 format"]
    pub dt: String,
    #[doc = " Unique ID of this event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[doc = " Unique ID of the request that caused the event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[doc = " Name of the stream/queue/dataset that this event belongs in"]
    pub stream: String,
    #[doc = " Unique URI identifying the event or entity"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uri: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageLinksChangePerformer {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc."]
    pub user_groups: Vec<String>,
    #[doc = " The user id that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    pub user_is_bot: bool,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_registration_dt: Option<String>,
    #[doc = " The text representation of the user that performed this change."]
    pub user_text: String,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct PageLinksChangeItemRemovedLinks {
    #[doc = " Whether link is external"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub external: Option<bool>,
    #[doc = " Link being removed"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageLinksChange {
    #[doc = " A URI identifying the JSONSchema for this event. This should match an schema's $id in a "]
    #[doc = " schema repository. E.g. /schema/title/1.0.0"]
    #[doc = ""]
    #[serde(rename = "$schema")]
    pub schema: String,
    #[doc = " The new page links. This map would only contain links that were either added or changed, "]
    #[doc = " links that were intact would not be present here. If the link was changed, its previous "]
    #[doc = " value would be present in the 'removed_links' object."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub added_links: Option<Vec<PageLinksChangeItemAddedLinks>>,
    #[doc = " The name of the wiki database this event entity belongs to."]
    pub database: String,
    pub meta: PageLinksChangeMeta,
    #[doc = " The (database) page ID."]
    pub page_id: i64,
    #[doc = " True if this page is currently a redirect page.  This fact is ultimately represented by "]
    #[doc = " revision content containing redirect wikitext.  If rev_id's content has redirect wikitext, "]
    #[doc = " then this page is a redirect.  Note that this state is also stored on the Mediawiki page "]
    #[doc = " table."]
    #[doc = ""]
    pub page_is_redirect: bool,
    #[doc = " The namespace ID this page belongs to."]
    pub page_namespace: i64,
    #[doc = " The normalized title of the page."]
    pub page_title: String,
    #[doc = " Represents the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub performer: Option<PageLinksChangePerformer>,
    #[doc = " The old page links. This map would only contain the previous values of the links that were "]
    #[doc = " either removed or changed by this event. Links that were intact would not be present here. "]
    #[doc = " If the link was changed, its new value would be present in the 'added_links' object."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub removed_links: Option<Vec<PageLinksChangeItemRemovedLinks>>,
    #[doc = " The head revision of the page at the time of this event."]
    pub rev_id: i64,
}
