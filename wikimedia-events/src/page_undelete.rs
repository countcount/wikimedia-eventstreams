use serde::{Deserialize, Serialize};

impl super::NormalEventData for PageUndelete {
    const SCHEMA: &'static str = "/mediawiki/page/undelete/1.0.0";
    const STREAM: &'static str = "mediawiki.page-undelete";

    fn event_schema(&self) -> &str {
        self.schema.as_str()
    }
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageUndeleteMeta {
    #[doc = " Domain the event or entity pertains to"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[doc = " UTC event datetime, in ISO-8601 format"]
    pub dt: String,
    #[doc = " Unique ID of this event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[doc = " Unique ID of the request that caused the event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[doc = " Name of the stream/queue/dataset that this event belongs in"]
    pub stream: String,
    #[doc = " Unique URI identifying the event or entity"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uri: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageUndeletePerformer {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc."]
    pub user_groups: Vec<String>,
    #[doc = " The user id that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    pub user_is_bot: bool,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_registration_dt: Option<String>,
    #[doc = " The text representation of the user that performed this change."]
    pub user_text: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageUndeletePriorState {
    #[doc = " The page ID before this restore as it was in the archive table."]
    pub page_id: i64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageUndelete {
    #[doc = " A URI identifying the JSONSchema for this event. This should match an schema's $id in a "]
    #[doc = " schema repository. E.g. /schema/title/1.0.0"]
    #[doc = ""]
    #[serde(rename = "$schema")]
    pub schema: String,
    #[doc = " Deprecated - no longer populated"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chronology_id: Option<String>,
    #[doc = " The comment left by the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " The name of the wiki database this event entity belongs to."]
    pub database: String,
    pub meta: PageUndeleteMeta,
    #[doc = " The (database) page ID."]
    pub page_id: i64,
    #[doc = " True if this page is currently a redirect page.  This fact is ultimately represented by "]
    #[doc = " revision content containing redirect wikitext.  If rev_id's content has redirect wikitext, "]
    #[doc = " then this page is a redirect.  Note that this state is also stored on the Mediawiki page "]
    #[doc = " table."]
    #[doc = ""]
    pub page_is_redirect: bool,
    #[doc = " The namespace ID this page belongs to."]
    pub page_namespace: i64,
    #[doc = " The normalized title of the page."]
    pub page_title: String,
    #[doc = " The comment left by the user that performed this change parsed into simple HTML. Optional"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parsedcomment: Option<String>,
    #[doc = " Represents the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub performer: Option<PageUndeletePerformer>,
    #[doc = " The prior state of the entity before this event. If a top level entity field is not present "]
    #[doc = " in this object, then its value has not changed since the prior event.  If prior_state "]
    #[doc = " itself is not present, then this event had no relevant prior state, indicating that it is "]
    #[doc = " probably the first time this type has been emitted for this entity. For page undeletes, "]
    #[doc = " prior_state will be absent unless the page_id is no longer the same as the page_id it had "]
    #[doc = " before it was deleted."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prior_state: Option<PageUndeletePriorState>,
    #[doc = " The head revision of the page at the time of this event."]
    pub rev_id: i64,
}
