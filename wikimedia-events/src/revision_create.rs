use serde::{Deserialize, Serialize};

impl super::NormalEventData for RevisionCreate {
    const SCHEMA: &'static str = "/mediawiki/revision/create/2.0.0";
    const STREAM: &'static str = "mediawiki.revision-create";

    fn event_schema(&self) -> &str {
        self.schema.as_str()
    }
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RevisionCreateMeta {
    #[doc = " Domain the event or entity pertains to"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[doc = " Time the event was received by the system, in UTC ISO-8601 format"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dt: Option<String>,
    #[doc = " Unique ID of this event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[doc = " Unique ID of the request that caused the event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[doc = " Name of the stream (dataset) that this event belongs in"]
    pub stream: String,
    #[doc = " Unique URI identifying the event or entity"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uri: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct RevisionCreatePerformer {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_groups: Option<Vec<String>>,
    #[doc = " The user id that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_is_bot: Option<bool>,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_registration_dt: Option<String>,
    #[doc = " The text representation of the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_text: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RevisionCreateRevRevertDetails {
    #[doc = " Flag indicating whether the revert was exact, i.e. the contents of the revert revision and "]
    #[doc = " restored revision match."]
    #[doc = ""]
    pub rev_is_exact_revert: bool,
    #[doc = " The ID of an earlier revision that is being repeated or restored."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_original_rev_id: Option<i64>,
    #[doc = " The method that was used to perform the revert."]
    pub rev_revert_method: String,
    #[doc = " IDs of revisions that were reverted by this edit, ordered from oldest to newest."]
    #[doc = ""]
    pub rev_reverted_revs: Vec<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct RevisionCreateRevSlotsMain {
    #[doc = " Model of the content (e.g. wikitext, wikibase-mediainfo...)"]
    pub rev_slot_content_model: String,
    #[doc = " Revision for which this slot was created"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_slot_origin_rev_id: Option<i64>,
    #[doc = " SHA1 of the slot content"]
    #[serde(rename = "rev_slot_sha1")]
    pub rev_slot_sha_1: String,
    #[doc = " Size in bytes of the slot content"]
    pub rev_slot_size: i64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RevisionCreateRevSlots {
    #[doc = " Schema fields describing a revision slot"]
    pub main: RevisionCreateRevSlotsMain,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct RevisionCreate {
    #[doc = " A URI identifying the JSONSchema for this event. This should match an schema's $id in a "]
    #[doc = " schema repository. E.g. /schema/title/1.0.0"]
    #[doc = ""]
    #[serde(rename = "$schema")]
    pub schema: String,
    #[doc = " Deprecated - no longer populated"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chronology_id: Option<String>,
    #[doc = " The comment left by the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " The name of the wiki database this event entity belongs to."]
    pub database: String,
    #[doc = " ISO-8601 formatted timestamp of when the event occurred/was generated in UTC), AKA 'event "]
    #[doc = " time'. This is different than meta.dt, which is used as the time the system received this "]
    #[doc = " event."]
    #[doc = ""]
    pub dt: String,
    pub meta: RevisionCreateMeta,
    #[doc = " The page ID of the page this revision belongs to."]
    pub page_id: i64,
    #[doc = " True if this revision is a redirect.  This fact is ultimately represented by revision "]
    #[doc = " content containing redirect wikitext.  If this revision is the head revision of the page, "]
    #[doc = " then the page will also be a redirect."]
    #[doc = ""]
    pub page_is_redirect: bool,
    #[doc = " The namespace of the page this revision belongs to."]
    pub page_namespace: i64,
    #[doc = " The normalized title of the page this revision belongs to."]
    pub page_title: String,
    #[doc = " The comment left by the user that performed this change parsed into simple HTML. Optional"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parsedcomment: Option<String>,
    #[doc = " Represents the user that performed this change."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub performer: Option<RevisionCreatePerformer>,
    #[doc = " True if the content has changed (rev_sha1 is different than the previous revision one)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_content_changed: Option<bool>,
    #[doc = " The content format of the revision."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_content_format: Option<String>,
    #[doc = " The content model of the revision."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_content_model: Option<String>,
    #[doc = " The (database) revision ID."]
    pub rev_id: i64,
    #[doc = " Flag indicating whether the edit was a revert."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_is_revert: Option<bool>,
    #[doc = " The length of the revision text in bytes."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_len: Option<i64>,
    #[doc = " Flag identifying if the revision is minor."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_minor_edit: Option<bool>,
    #[doc = " The parent revison ID of the revision that this event represents."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_parent_id: Option<i64>,
    #[doc = " Details about the revert."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_revert_details: Option<RevisionCreateRevRevertDetails>,
    #[doc = " The sha1 of the revision text."]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "rev_sha1")]
    pub rev_sha_1: Option<String>,
    #[doc = " The revision slots attached to this revision."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_slots: Option<RevisionCreateRevSlots>,
    #[doc = " The revision's creation time in ISO8601 format.  This field does not end in '_dt' to better "]
    #[doc = " match the field name on the Mediawiki revision table."]
    #[doc = ""]
    pub rev_timestamp: String,
}
