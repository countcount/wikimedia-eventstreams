use serde::{Deserialize, Serialize};

impl super::NormalEventData for PageChange {
    const SCHEMA: &'static str = "/mediawiki/page/change/1.2.0";
    const STREAM: &'static str = "mediawiki.page_change.v1";

    fn event_schema(&self) -> &str {
        self.schema.as_str()
    }
}

#[doc = " The number of revisions of this page at the time of this event. During a delete, this number of "]
#[doc = " revisions will be archived. This field is likely only set for page delete events, as getting "]
#[doc = " this information on all events is expensive."]
#[doc = ""]
pub type RevisionCount = i64;
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangeCreatedRedirectPage {
    #[doc = " True if the page is a redirect page at the time of this event."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_redirect: Option<bool>,
    #[doc = " The id of the namespace this page belongs to."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace_id: Option<i64>,
    #[doc = " The (database) page ID of the page."]
    pub page_id: i64,
    #[doc = " The normalized title of the page."]
    pub page_title: String,
    #[doc = " NOTE: revision_count is never set for created_redirect_page. It is present here for "]
    #[doc = " backwards compatibility only."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision_count: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct PageChangeMeta {
    #[doc = " Domain the event or entity pertains to"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[doc = " Time the event was received by the system, in UTC ISO-8601 format"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dt: Option<String>,
    #[doc = " Unique ID of this event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[doc = " Unique ID of the request that caused the event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[doc = " Name of the stream (dataset) that this event belongs in"]
    pub stream: String,
    #[doc = " Unique URI identifying the event or entity"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uri: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePageRedirectPageLink {
    #[doc = " The interwiki prefix (iw_prefix) of this link. The presence of this prefix implies a target "]
    #[doc = " outside the local wiki. See https://meta.wikimedia.org/wiki/Help:Interwiki_linking"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub interwiki_prefix: Option<String>,
    #[doc = " True if the page is a redirect page at the time of this event."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_redirect: Option<bool>,
    #[doc = " The id of the namespace this page belongs to."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace_id: Option<i64>,
    #[doc = " The (database) page ID of the page."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_id: Option<i64>,
    #[doc = " The normalized title of the page."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_title: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePage {
    #[doc = " True if the page is a redirect page at the time of this event."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_redirect: Option<bool>,
    #[doc = " The id of the namespace this page belongs to."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace_id: Option<i64>,
    #[doc = " The (database) page ID of the page."]
    pub page_id: i64,
    #[doc = " The normalized title of the page."]
    pub page_title: String,
    #[doc = " If this page is currently a redirect, then this field contains information about the target "]
    #[doc = " page the redirect links to."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redirect_page_link: Option<PageChangePageRedirectPageLink>,
    #[doc = " The number of revisions of this page at the time of this event. During a delete, this "]
    #[doc = " number of revisions will be archived. This field is likely only set for page delete events, "]
    #[doc = " as getting this information on all events is expensive."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision_count: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePerformer {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_bot: Option<bool>,
    #[doc = " True if the user is a MediaWiki 'system' user. These are users that cannot 'authenticate'.  "]
    #[doc = " These are usually listed in ReservedUsernames."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_system: Option<bool>,
    #[doc = " True if the user is an autocreated temporary MediaWiki user. This is used for IP masking."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_temp: Option<bool>,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registration_dt: Option<String>,
    #[doc = " The user ID that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " The user name or text representation of the user that performed this change."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_text: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePriorStatePage {
    #[doc = " True if the page is a redirect page at the time of this event."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_redirect: Option<bool>,
    #[doc = " The id of the namespace this page belongs to."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace_id: Option<i64>,
    #[doc = " The (database) page ID of the page."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_id: Option<i64>,
    #[doc = " The normalized title of the page."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_title: Option<String>,
    #[doc = " NOTE: prior_state.page.revision_count is unlikely to be set, as getting the # of revisions "]
    #[doc = " previous to this change is difficult. This field is present here for backwards "]
    #[doc = " compatibiliy."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision_count: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePriorStateRevisionContentSlots {
    #[doc = " Content body. NOTE: This field is not required, and is often not set in streams as it can "]
    #[doc = " make events very large. It is included here for events that do include the content body."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_body: Option<String>,
    #[doc = " The 'content type' of the content.  E.g. wikitext/html. This is similiar to a MIME type."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_format: Option<String>,
    #[doc = " MediaWiki's content model of this content. E.g. wikitext, json, etc."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_model: Option<String>,
    #[doc = " sha1 sum of the content body."]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "content_sha1")]
    pub content_sha_1: Option<String>,
    #[doc = " Byte size of the content body."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_size: Option<i64>,
    #[doc = " Revision in which this slot was originally created"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub origin_rev_id: Option<i64>,
    #[doc = " Slot role name."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slot_role: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePriorStateRevisionEditor {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_bot: Option<bool>,
    #[doc = " True if the user is a MediaWiki 'system' user. These are users that cannot 'authenticate'.  "]
    #[doc = " These are usually listed in ReservedUsernames."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_system: Option<bool>,
    #[doc = " True if the user is an autocreated temporary MediaWiki user. This is used for IP masking."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_temp: Option<bool>,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registration_dt: Option<String>,
    #[doc = " The user ID that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " The user name or text representation of the user that performed this change."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_text: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangePriorStateRevision {
    #[doc = " The comment left by the editor when this revision was made."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " Map type representing MediaWiki's revision content slots. This map is keyed by the slot "]
    #[doc = " role name, e.g. 'main'."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_slots:
        Option<::std::collections::BTreeMap<String, PageChangePriorStateRevisionContentSlots>>,
    #[doc = " Represents the MediaWiki user that made this edit."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub editor: Option<PageChangePriorStateRevisionEditor>,
    #[doc = " Whether the comment of the revision is visible. See RevisionRecord->DELETED_COMMENT."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_comment_visible: Option<bool>,
    #[doc = " Whether the revision's content body is visible. If this is false, then content should be "]
    #[doc = " redacted. See RevisionRecord->DELETED_TEXT"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_content_visible: Option<bool>,
    #[doc = " Whether the revision's editor information is visible. Affects editor field. See "]
    #[doc = " RevisionRecord->DELETED_USER"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_editor_visible: Option<bool>,
    #[doc = " True if the editor marked this revision as a minor edit."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_minor_edit: Option<bool>,
    #[doc = " Time this revision was created. This is rev_timestamp in the MediaWiki database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_dt: Option<String>,
    #[doc = " The (database) revision ID."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_id: Option<i64>,
    #[doc = " This revision's parent rev_id."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_parent_id: Option<i64>,
    #[doc = " sha1 sum considering all the content slots for this revision."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "rev_sha1")]
    pub rev_sha_1: Option<String>,
    #[doc = " Byte size 'sum' of all the content slots for this revision. This 'size' is approximate, but "]
    #[doc = " may not be exact, depending on the kind of data that is stored in the content slots."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_size: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct PageChangePriorState {
    #[doc = " Fields for MediaWiki page entity."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub page: Option<PageChangePriorStatePage>,
    #[doc = " Fields for MediaWiki revision entity."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision: Option<PageChangePriorStateRevision>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangeRevisionContentSlots {
    #[doc = " Content body. NOTE: This field is not required, and is often not set in streams as it can "]
    #[doc = " make events very large. It is included here for events that do include the content body."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_body: Option<String>,
    #[doc = " The 'content type' of the content.  E.g. wikitext/html. This is similiar to a MIME type."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_format: Option<String>,
    #[doc = " MediaWiki's content model of this content. E.g. wikitext, json, etc."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_model: Option<String>,
    #[doc = " sha1 sum of the content body."]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "content_sha1")]
    pub content_sha_1: Option<String>,
    #[doc = " Byte size of the content body."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_size: Option<i64>,
    #[doc = " Revision in which this slot was originally created"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub origin_rev_id: Option<i64>,
    #[doc = " Slot role name."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub slot_role: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangeRevisionEditor {
    #[doc = " The number of edits this user has made at the time of this event. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub edit_count: Option<i64>,
    #[doc = " A list of the groups this user belongs to.  E.g. bot, sysop etc. Not present for anonymous "]
    #[doc = " users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[doc = " True if this user is considered to be a bot at the time of this event. This is checked via "]
    #[doc = " the $user->isBot() method, which considers both user_groups and user permissions."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_bot: Option<bool>,
    #[doc = " True if the user is a MediaWiki 'system' user. These are users that cannot 'authenticate'.  "]
    #[doc = " These are usually listed in ReservedUsernames."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_system: Option<bool>,
    #[doc = " True if the user is an autocreated temporary MediaWiki user. This is used for IP masking."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_temp: Option<bool>,
    #[doc = " The datetime of the user account registration. Not present for anonymous users or if "]
    #[doc = " missing in the MW database."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub registration_dt: Option<String>,
    #[doc = " The user ID that performed this change.  This is optional, and will not be present for "]
    #[doc = " anonymous users."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    #[doc = " The user name or text representation of the user that performed this change."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_text: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChangeRevision {
    #[doc = " The comment left by the editor when this revision was made."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " Map type representing MediaWiki's revision content slots. This map is keyed by the slot "]
    #[doc = " role name, e.g. 'main'."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_slots: Option<::std::collections::BTreeMap<String, PageChangeRevisionContentSlots>>,
    #[doc = " Represents the MediaWiki user that made this edit."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub editor: Option<PageChangeRevisionEditor>,
    #[doc = " Whether the comment of the revision is visible. See RevisionRecord->DELETED_COMMENT."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_comment_visible: Option<bool>,
    #[doc = " Whether the revision's content body is visible. If this is false, then content should be "]
    #[doc = " redacted. See RevisionRecord->DELETED_TEXT"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_content_visible: Option<bool>,
    #[doc = " Whether the revision's editor information is visible. Affects editor field. See "]
    #[doc = " RevisionRecord->DELETED_USER"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_editor_visible: Option<bool>,
    #[doc = " True if the editor marked this revision as a minor edit."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_minor_edit: Option<bool>,
    #[doc = " Time this revision was created. This is rev_timestamp in the MediaWiki database."]
    #[doc = ""]
    pub rev_dt: String,
    #[doc = " The (database) revision ID."]
    pub rev_id: i64,
    #[doc = " This revision's parent rev_id."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_parent_id: Option<i64>,
    #[doc = " sha1 sum considering all the content slots for this revision."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "rev_sha1")]
    pub rev_sha_1: Option<String>,
    #[doc = " Byte size 'sum' of all the content slots for this revision. This 'size' is approximate, but "]
    #[doc = " may not be exact, depending on the kind of data that is stored in the content slots."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rev_size: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageChange {
    #[doc = " A URI identifying the JSONSchema for this event. This should match an schema's $id in a "]
    #[doc = " schema repository. E.g. /schema/title/1.0.0"]
    #[doc = ""]
    #[serde(rename = "$schema")]
    pub schema: String,
    #[doc = " The kind of this event in a changelog. This is used to map the event to an action in a data "]
    #[doc = " store."]
    #[doc = ""]
    pub changelog_kind: String,
    #[doc = " The comment left by the user that performed this change. Same as revision.comment on edits."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " Page entity that was created at the old title during a page move. This is only set for page "]
    #[doc = " move events. Note that the created_redirect_page will also have its own associated page "]
    #[doc = " create event."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_redirect_page: Option<PageChangeCreatedRedirectPage>,
    #[doc = " ISO-8601 formatted timestamp of when the event occurred/was generated in UTC), AKA 'event "]
    #[doc = " time'. This is different than meta.dt, which is used as the time the system received this "]
    #[doc = " event."]
    #[doc = ""]
    pub dt: String,
    pub meta: PageChangeMeta,
    #[doc = " Fields for MediaWiki page entity."]
    pub page: PageChangePage,
    #[doc = " The origin kind of the change to this page as viewed by MediaWiki."]
    #[doc = ""]
    pub page_change_kind: String,
    #[doc = " Represents the MediaWiki actor that made this change. If this change is an edit, this will "]
    #[doc = " be the same as revision.editor."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub performer: Option<PageChangePerformer>,
    #[doc = " Prior state of this page before this event. Fields are only present if their values have "]
    #[doc = " changed."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prior_state: Option<PageChangePriorState>,
    #[doc = " Fields for MediaWiki revision entity."]
    pub revision: PageChangeRevision,
    #[doc = " The wiki ID, which is usually the same as the MediaWiki database name. E.g. enwiki, "]
    #[doc = " metawiki, etc."]
    #[doc = ""]
    pub wiki_id: String,
}
