use serde::{
    de::{DeserializeOwned, Error},
    Deserialize,
};
use std::fmt::Debug;

use crate::{
    page_change::PageChange, page_create::PageCreate, page_delete::PageDelete,
    page_links_change::PageLinksChange, page_move::PageMove,
    page_properties_change::PagePropertiesChange, page_undelete::PageUndelete,
    recentchange::RecentChange, revision_create::RevisionCreate,
    revision_tags_change::RevisionTagsChange, revision_visibility_change::RevisionVisibilityChange,
};

pub mod page_change;
pub mod page_create;
pub mod page_delete;
pub mod page_links_change;
pub mod page_move;
pub mod page_properties_change;
pub mod page_undelete;
pub mod recentchange;
pub mod revision_create;
pub mod revision_tags_change;
pub mod revision_visibility_change;

pub trait EventData: Debug + DeserializeOwned + 'static {}

pub trait NormalEventData: EventData {
    const SCHEMA: &'static str;
    const STREAM: &'static str;

    fn event_schema(&self) -> &str;
}

impl<T> EventData for T where T: NormalEventData {}

#[derive(Clone, Debug)]
#[non_exhaustive]
pub enum Any {
    PageChange(PageChange),
    PageCreate(PageCreate),
    PageDelete(PageDelete),
    PageLinksChange(PageLinksChange),
    PageMove(PageMove),
    PagePropertiesChange(PagePropertiesChange),
    PageUndelete(PageUndelete),
    RecentChange(RecentChange),
    RevisionCreate(RevisionCreate),
    RevisionTagsChange(RevisionTagsChange),
    RevisionVisibilityChange(RevisionVisibilityChange),
}

impl EventData for Any {}

/// custom deserializer for Any that deserializes based on the meta/stream field
impl<'de> Deserialize<'de> for Any {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let map = serde_json::Map::deserialize(deserializer)?;

        let stream = map
            .get("meta")
            .ok_or_else(|| Error::missing_field("meta"))?
            .as_object()
            .ok_or_else(|| Error::custom("meta must be a map"))?
            .get("stream")
            .ok_or_else(|| Error::missing_field("meta/stream"))?
            .as_str()
            .ok_or_else(|| Error::custom("meta/stream must a be string"))?;

        macro_rules! event {
            ($i:ident) => {{
                let ev = $i::deserialize(serde_json::Value::Object(map)).map_err(Error::custom)?;
                Ok(Any::$i(ev))
            }};
        }

        match stream {
            PageChange::STREAM => event!(PageChange),
            PageCreate::STREAM => event!(PageCreate),
            PageDelete::STREAM => event!(PageDelete),
            PageLinksChange::STREAM => event!(PageLinksChange),
            PageMove::STREAM => event!(PageMove),
            PagePropertiesChange::STREAM => event!(PagePropertiesChange),
            PageUndelete::STREAM => event!(PageUndelete),
            RecentChange::STREAM => event!(RecentChange),
            RevisionCreate::STREAM => event!(RevisionCreate),
            RevisionTagsChange::STREAM => event!(RevisionTagsChange),
            RevisionVisibilityChange::STREAM => event!(RevisionVisibilityChange),
            _ => Err(Error::custom(format!("unknown stream: {}", stream))),
        }
    }
}
