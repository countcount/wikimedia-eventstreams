use serde::{Deserialize, Serialize};

impl super::NormalEventData for RecentChange {
    const SCHEMA: &'static str = "/mediawiki/recentchange/1.0.1";
    const STREAM: &'static str = "mediawiki.recentchange";

    fn event_schema(&self) -> &str {
        self.schema.as_str()
    }
}

#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct RecentChangeLength {
    #[doc = " (rc_new_len)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new: Option<i64>,
    #[doc = " (rc_old_len)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RecentChangeMeta {
    #[doc = " Domain the event or entity pertains to"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[doc = " UTC event datetime, in ISO-8601 format"]
    pub dt: String,
    #[doc = " Unique ID of this event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[doc = " Unique ID of the request that caused the event"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[doc = " Name of the stream/queue/dataset that this event belongs in"]
    pub stream: String,
    #[doc = " Unique URI identifying the event or entity"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub uri: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct RecentChangeRevision {
    #[doc = " (rc_last_oldid)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new: Option<i64>,
    #[doc = " (rc_this_oldid)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub old: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct RecentChange {
    #[doc = " A URI identifying the JSONSchema for this event. This should match an schema's $id in a "]
    #[doc = " schema repository. E.g. /schema/title/1.0.0"]
    #[doc = ""]
    #[serde(rename = "$schema")]
    pub schema: String,
    #[doc = " (rc_bot)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bot: Option<bool>,
    #[doc = " (rc_comment)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
    #[doc = " ID of the recentchange event (rcid)."]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[doc = " Length of old and new change"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub length: Option<RecentChangeLength>,
    #[doc = " (rc_log_action)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_action: Option<String>,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_action_comment: Option<String>,
    #[doc = " (rc_log_id)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_id: Option<i64>,
    #[doc = " Property only exists if event has rc_params."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_params: Option<serde_json::Value>,
    #[doc = " (rc_log_type)"]
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub log_type: Option<String>,
    pub meta: RecentChangeMeta,
    #[doc = " (rc_minor)."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub minor: Option<bool>,
    #[doc = " ID of relevant namespace of affected page (rc_namespace, page_namespace). This is -1 "]
    #[doc = " (\"Special\") for log events."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub namespace: Option<i64>,
    #[doc = " The rc_comment parsed into simple HTML. Optional"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parsedcomment: Option<String>,
    #[doc = " (rc_patrolled). This property only exists if patrolling is supported for this event (based "]
    #[doc = " on $wgUseRCPatrol, $wgUseNPPatrol)."]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub patrolled: Option<bool>,
    #[doc = " Old and new revision IDs"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision: Option<RecentChangeRevision>,
    #[doc = " $wgServerName"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub server_name: Option<String>,
    #[doc = " $wgScriptPath"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub server_script_path: Option<String>,
    #[doc = " $wgCanonicalServer"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub server_url: Option<String>,
    #[doc = " Unix timestamp (derived from rc_timestamp)."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<i64>,
    #[doc = " Full page name, from Title::getPrefixedText."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[doc = " Type of recentchange event (rc_type). One of \"edit\", \"new\", \"log\", \"categorize\", or "]
    #[doc = " \"external\"; or a number. (See Manual:Recentchanges table#rc_type)"]
    #[doc = ""]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[doc = " (rc_user_text)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
    #[doc = " wfWikiID ($wgDBprefix, $wgDBname)"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wiki: Option<String>,
}
